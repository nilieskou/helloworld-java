# Dockerfile.temp
FROM registry.gitlab.com/smoht/gemnasium-maven

# Use your custom base image as the base
FROM my-base-image

# Use Amazon Corretto 18.0.2 on Amazon Linux 2 as the base image
FROM amazoncorretto:18.0.2-al2

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy the Java application JAR file into the container
COPY HelloWorld.class .

# Run the Java application when the container starts
CMD ["java", "HelloWorld"]